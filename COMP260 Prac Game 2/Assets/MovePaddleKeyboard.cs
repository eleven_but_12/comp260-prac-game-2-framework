﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeyboard : MonoBehaviour
{
    private Rigidbody rigidbody;
    public float speed = 20f;
    //public float force = 20f;
    public string axisX;
    public string axisY;
    private Vector3 direction;

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        direction.x = Input.GetAxis(axisX);
        direction.z = Input.GetAxis(axisY);


        //Vector3 pos = GetMousePosition();
        //Vector3 dir = pos - rigidbody.position;
        Vector3 vel = direction.normalized * speed;

        // check is this speed is going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = direction.magnitude;

        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }

        rigidbody.velocity = vel;
    }


    //void FixedUpdate()
    //{
    //  Vector3 pos = GetMousePosition();
    // Vector3 dir = pos - rigidbody.position;

    // rigidbody.AddForce(dir.normalized * force);
    // }
}
