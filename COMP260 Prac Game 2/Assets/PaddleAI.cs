﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleAI : MonoBehaviour {

    private Rigidbody rigidbody;
    //private Vector3 velocity;
    //public Vector3 puck;
    //public Vector3 goal;
    public Transform target;
    public Transform goalPos;
    private Vector3 midpoint = new Vector3(0,0,0);
    public float speed = 15f;

	// Use this for initialization
	void Start ()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
	}

    //public void ReturnToGoal()
    //{
    //    Vector3 start = new Vector3(4.5f, 0f, 0f);
    //    if (rigidbody.position != start)
    //    { Vector3 direction = goalPos.position - rigidbody.position;
    //        direction = direction.normalized;

    //        Vector3 velocity = direction * speed;
    //        transform.Translate(velocity * Time.deltaTime);
    //    }
    //}

    void FixedUpdate()
    {
        if (target.position == midpoint) {
            Vector3 direction = target.position - rigidbody.position;
            direction = direction.normalized;

            Vector3 velocity = direction * speed;
            transform.Translate(velocity * Time.deltaTime);
        }

        //if(rigidbody.position == midpoint)
        //{
           //ReturnToGoal();
        //}

        //if (rigidbody.position == midpoint)
        //{
        //    Vector3 start = new Vector3(4.5f, 0f, 0f);
        //    //velocity = (Vector3.zero);
        //    transform.Translate(start * Time.deltaTime);
        //}

        //goal = goalPos;
        //midpoint = (puck + goal) / 2;
        //Debug.Log("Midpoint: " + midpoint);
    }

    // Update is called once per frame
    }
