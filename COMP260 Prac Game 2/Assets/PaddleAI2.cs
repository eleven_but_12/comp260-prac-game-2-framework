﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class PaddleAI2 : MonoBehaviour
{
    private Rigidbody rigidbody;
    public float speed = 20f;
    //public float force = 20f;
    //public string axisX;
    //public string axisY;
    public Transform target;
    public Transform start;
    public Vector3 start1;
    public Vector3 target1;
    //private Vector3 start1 = new Vector3(4.5f, 0, 0);
    //private Vector3 target1 = new Vector3(0, 0, 0);
    private Vector3 pos;
    private Vector3 direction;
    //static float t = 0.0f;
    float count = 0.0f;
    float percent = 0.0f;

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {

        //count+= 0.5f * Time.deltaTime;
        //if (count > 2)
        //{
        //    count = 0;
        //}
        //else if ( count <=1)
        //{
        //    percent = count;
        //}
        //else if (count > 1)
        //{
        //    percent = 2 - count;
        //}

        pos = Vector3.Lerp(target1, start1, percent);
        rigidbody.velocity = pos;


        //rigidbody.velocity = new Vector3(Mathf.Lerp(max, min, -t), 0, 0);

        //t -= 0.5f * Time.deltaTime;

        //if (t < 0.0f)
        //{
        //    float temp = max;
        //    max = min;
        //    min = temp;
        //    t = 0.0f;
        //}


        //Vector3.Lerp(start.position, target.position, speed) * Time.deltaTime;
        //direction.x = Input.GetAxis(axisX);
        //direction.z = Input.GetAxis(axisY);


        //Vector3 pos = GetMousePosition();
        //Vector3 dir = pos - rigidbody.position;
        //direction = target.position - rigidbody.position;

        //Vector3 vel = direction.normalized * speed;

        // check is this speed is going to overshoot the target
        //float move = speed * Time.fixedDeltaTime;
        //float distToTarget = direction.magnitude;

        //if (move > distToTarget)
        //{
        //    // scale the velocity down appropriately
        //    vel = vel * distToTarget / move;
        //}

        //rigidbody.velocity = vel;
    }


    //void FixedUpdate()
    //{
    //  Vector3 pos = GetMousePosition();
    // Vector3 dir = pos - rigidbody.position;

    // rigidbody.AddForce(dir.normalized * force);
    // }
}
