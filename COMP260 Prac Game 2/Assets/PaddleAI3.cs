﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleAI3 : MonoBehaviour
{

    private Rigidbody rigidbody;
    //private Vector3 velocity;
    //public Vector3 puck;
    //public Vector3 goal;
    public Transform target;
    public Transform goalPos;
    private Vector3 midpoint = new Vector3(0, 0, 0);
    private Vector3 start1 = new Vector3(4.5f, 0, 0);
    public float speed = 15f;

    public float min = 0.0F;
    public float max = 4.5F;

    static float t = 0.0f;

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;

    }

    void FixedUpdate()
    {
        rigidbody.position = new Vector3(Mathf.Lerp(max, min, t), 0, 0);

        t += 0.5f * Time.deltaTime;

        if (t > 1.0f)
        {
            float temp = max;
            max = min;
            min = temp;
            t = 0.0f;
        }

        //if (rigidbody.position == start1)
        //{
        //    rigidbody.velocity = -start1;
        //}
        //else if (rigidbody.position == midpoint)
        //{
        //    rigidbody.velocity = start1;
        //}


        //rigidbody.velocity = Vector3.Lerp(start1, midpoint, 1.0f);

        //if (target.position == midpoint)
        //{
        //    Vector3 direction = target.position - rigidbody.position;
        //    direction = direction.normalized;

        //    Vector3 velocity = direction * speed;
        //    transform.Translate(velocity * Time.deltaTime);
        //}

    }
}
